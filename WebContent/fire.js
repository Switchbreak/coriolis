/**
 * 
 */

var centerVector = new THREE.Vector3();
var lookMatrix = new THREE.Matrix4();

var fire = function( position ) {
	/** @type THREE.Mesh */
	var mesh = null;
	/** @type THREE.MeshFaceMaterial */
	var material = null;
	
	var timer = 0.1;
	
	function init() {
		material = new THREE.MeshFaceMaterial(sharedData.loadedMaterials["resources/Fire.json"]);
		mesh = new THREE.Mesh(sharedData.loadedGeometries["resources/Fire.json"], material)
		
		mesh.scale.set( 20, 20, 20 );
		mesh.position.copy( position );
		
		centerVector.set(0, 0, mesh.position.z);
		centerVector.sub(mesh.position).normalize();
		lookMatrix.lookAt( mesh.position, mesh.position, centerVector );
		mesh.quaternion.setFromRotationMatrix( lookMatrix );
		
		sharedData.scene.add( mesh );
		
		sharedData.light.position.addVectors( mesh.position, centerVector.multiplyScalar(200) );
		sharedData.light.visible = true;
	}
	
	this.update = function( delta ) {
		timer -= delta;
		
		if( timer <= 0 ) {
			sharedData.light.intensity = Math.random() * 0.2 + 0.8;
			timer = 0.1;
		}
	};
	
	init();
}