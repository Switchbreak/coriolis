/** @constant */
var EPSILON = 0.00001;

/**
 * Checks for collision moving a point along a vector
 * @param {THREE.Vector3} origin - Starting position
 * @param {THREE.Vector3} vector - Movement vector
 * @param {Array} entities - List of entities to check for collisions
 * @returns {Object} - Null if no collision found, otherwise the point of collision
 */
function CollisionCheck( origin, vector, entities ) {
	var minParameter = Infinity;
	
	for( var i = 0; i < entities.length; i++ ) {
		var collision = SegmentMeshIntersection(origin, vector, entities[i]);
		if( collision && collision.parameter < minParameter )
			return collision;
	}
	
	return null;
}

var intersectionPoint = new THREE.Vector3();

/**
 * Checks for intersection between a line segment and a mesh
 * @param {THREE.Vector3} origin - Line segment defined by origin and vector
 * @param {THREE.Vector3} vector
 * @param {THREE.Mesh} mesh - Mesh to check for collisions
 * @returns {Object} - Null if no collision found, otherwise the point of collision
 */
function SegmentMeshIntersection( origin, vector, mesh ) {
	/** @type THREE.Geometry */
	var geometry = mesh.geometry;
	var vertices = geometry.vertices;
	
	var minZ, maxZ;
	if( vector.z > 0 ) {
		minZ = origin.z;
		maxZ = origin.z + vector.z;
	}
	else {
		minZ = origin.z + vector.z;
		maxZ = origin.z;
	}
	
	var startIndex = Math.floor(minZ / 100) * 198;
	var endIndex = Math.ceil(maxZ / 100) * 198;
	
	var minParameter = Infinity;
	var collision = null
	
	for( var index = startIndex; index < endIndex; index++ ) {
		/** @type THREE.Face3 */
		var face = geometry.faces[index];
		
		var a = vertices[face.a];
		var b = vertices[face.b];
		var c = vertices[face.c];
		
		var parameter = SegmentPlaneIntersection(origin, vector, a, face.normal);
		if( parameter && parameter < minParameter ) {
			intersectionPoint.copy(vector).multiplyScalar(parameter).add(origin);
			
			if( PointInTriangle(intersectionPoint, a, b, c) )
				collision = { intersectionPoint: intersectionPoint, face: face, parameter: parameter };
		}
	}
	
	return collision;
}

/**
 * <p>Determines parametric point of intersection r along line p0 -> p1
 * with a plane going through v0 with normal n, using the following equation:</p>
 * <p>r = (n * (v0 - p0)) / (n * (p1 - p0))</p>
 * <p>Segment intersects if line p0 -> p1 is not parallel with the plane and
 * r(p1 - p0) is within the segment</p>
 * 
 * @param {THREE.Vector3} endPoint0 - Line segment defined by origin and vector
 * @param {THREE.Vector3} endPoint1
 * @param {THREE.Vector3} v0		- Plane defined by point v0 inside the plane and normal vector
 * @param {THREE.Vector3} normal
 * @returns {Number} - Parametric point of intersection
 */
function SegmentPlaneIntersection( origin, vector, v0, normal ) {
	var planeVector = new THREE.Vector3();
	
	planeVector.subVectors(v0, origin);
	
	var denominator = normal.dot( vector );
	if( denominator == 0 )
		return null;				// Line segment and plane are parallel, no intersection
	
	var parameter = normal.dot( planeVector ) / denominator;
	if( parameter < -EPSILON || parameter > 1 + EPSILON )
		return null;
	
	return parameter;
}

/**
 * <p>Determines intersection by calculating barycentric coordinates (s, t)
 * with the following equation:</p>
 * 
 * <blockquote>
 * <p>Where u and v are vectors along two triangle sides from v0<br />
 * Where w is a vector from v0 to point</p>
 * 
 * <p>s = ((u * v)(w * v) - (v * v)(w * u)) / ((u * v) ^ 2 - (u * u)(v * v))<br />
 * t = ((u * v)(w * u) - (u * u)(w * v)) / ((u * v) ^ 2 - (u * u)(v * v))</p>
 * </blockquote>
 * 
 * <p>Pulled from: {@link http://geomalgorithms.com/a06-_intersect-2.html}</p>
 * 
 * @param {THREE.Vector3} point - Test point
 * @param {THREE.Vector3} v0 - Triangle defined by points v0, v1, v2
 * @param {THREE.Vector3} v1
 * @param {THREE.Vector3} v2
 * @returns {Boolean}
 */
var u = new THREE.Vector3();
var v = new THREE.Vector3();
var w = new THREE.Vector3();

function PointInTriangle( point, v0, v1, v2 ) {
	u.subVectors(v1, v0);
	v.subVectors(v2, v0);
	w.subVectors(point, v0);
	
	var UdotU = u.dot(u);
	var VdotV = v.dot(v);
	var UdotV = u.dot(v);
	var WdotV = w.dot(v);
	var WdotU = w.dot(u);
	
	var denominator = (UdotV * UdotV - UdotU * VdotV);
	
	var s = (UdotV * WdotV - VdotV * WdotU) / denominator;
	var t = (UdotV * WdotU - UdotU * WdotV) / denominator;
	
	if( s >= -EPSILON && t >= -EPSILON && s + t <= 1 + EPSILON )
		return true;
	else
		return false;
}