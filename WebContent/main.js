var clock = new THREE.Clock();

var sharedData = {
	/** @type THREE.Scene */
	scene : null,
	/** @type THREE.PerspectiveCamera */
	camera : null,
	/** @type THREE.WebGLRenderer */
	renderer : null,
	/** @type Stats */
	stats : null,
	/** @type inputEngine */
	controls : null,
	/** @type FlyCamera */
	cameraControls : null,
	/** @type UnitControl */
	unitControls : null,
	/** @type Array */
	physicsObjects : [],
	loadedGeometries : [],
	loadedMaterials : [],
	landscape : null,
	/** @type THREE.PointLight */
	pointLight: null,
};

var entities = [];
var staticEntities = [];
var resources = ["resources/Tree.json",
                 "resources/Astronaut.json",
                 "resources/SpaceRamps.json",
                 "resources/Tent.json",
                 "resources/Fire.json",
                 ];
var resourcesLoaded = 0;

init();
animate();

function init() {
	sharedData.scene = new THREE.Scene();
	sharedData.scene.fog = new THREE.FogExp2(0xaaaaaa, 0.00025);

	loadGeometries();
	initCamera();
	initLights();
	initRenderer();
	initControls();
	initStats();
}

function loadGeometries() {
	var loader = new THREE.JSONLoader();
	
	for( var i = 0; i < resources.length; i++ ) {
		loader.load( resources[i],
				function () {
					var name = resources[i];
					return function(geometry, materials) {
						onGeometryLoaded( name, geometry, materials );
					}
				}(),
				function ( xhr ) {						// Function called when download progresses
					console.log( (xhr.loaded / xhr.total * 100) + '% loaded' );
				} );
	}
}

function onGeometryLoaded( name, geometry, materials ) {
	sharedData.loadedGeometries[name] = geometry;
	if( materials )
		sharedData.loadedMaterials[name] = materials;
	
	resourcesLoaded++;
	
	if( resourcesLoaded == resources.length )
		initEntities();
}

function initEntities() {
	sharedData.landscape = new landscape();
	staticEntities.push(sharedData.landscape);
	staticEntities.push(new ramps());
	entities.push(new villager());
	
	for( var i = 0; i < 500; i++ )
		staticEntities.push(new tree());
	
	window.setTimeout(function() {
		sharedData.light.visible = false;
		}, 500);
}

function initCamera() {
	sharedData.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 10000);
}

function initLights() {
	sharedData.scene.add(new THREE.AmbientLight(0x404040));
	
	sharedData.light = new THREE.PointLight(0xcca10a, 1, 1000)
	sharedData.light.visible = true;
	sharedData.scene.add( sharedData.light );

	var light = new THREE.DirectionalLight(0xffffff);
	light.position.set(1, 1, 1).normalize();
	sharedData.scene.add(light);
}

function initRenderer() {
	sharedData.renderer = new THREE.WebGLRenderer();
	sharedData.renderer.setSize(window.innerWidth - 10, window.innerHeight - 21);
	sharedData.renderer.setClearColor(0xaaaaaa);

	document.body.appendChild(sharedData.renderer.domElement);
}

function initControls() {
	sharedData.controls = new inputEngine();
	sharedData.cameraControls = new FlyCamera( sharedData.camera, sharedData.controls );
	
	sharedData.unitControls = new UnitControl();
}

function initStats() {
	sharedData.stats = new Stats();
	sharedData.stats.setMode(0);

	sharedData.stats.domElement.style.position = 'absolute';
	sharedData.stats.domElement.style.left = '0px';
	sharedData.stats.domElement.style.top = '0px';

	document.body.appendChild(sharedData.stats.domElement);
}

function animate() {
	requestAnimationFrame(animate);

	sharedData.stats.begin();
	var delta = clock.getDelta();

	sharedData.controls.update();
	sharedData.unitControls.update();
	sharedData.cameraControls.update( delta );
	for (var i = 0; i < entities.length; i++) {
		entities[i].update(delta);
	}
	
	THREE.AnimationHandler.update(delta);

	sharedData.renderer.render(sharedData.scene, sharedData.camera);
	sharedData.stats.end();
}