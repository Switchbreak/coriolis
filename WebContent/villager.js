var villager = function() {
	this.mesh = null;
	
	var material = new THREE.MeshLambertMaterial({ color: 0xffffff });
	var velocity = new THREE.Vector3( 0, 0, 0 );
	
	this.init = function() {
		this.mesh = new THREE.Mesh(sharedData.loadedGeometries["resources/Astronaut.json"], material);
		
		this.mesh.scale.set( 5, 5, -5 );
		
		this.mesh.position.x = Math.random() * 20 - 10;
		this.mesh.position.y = Math.random() * 20 - 10;
		this.mesh.position.z = Math.random() * 5000 + 2500;
		
		sharedData.scene.add( this.mesh );
	}
	
	var centerPoint = new THREE.Vector3();
	var gravityVector = new THREE.Vector3();
	var scaledGravity = new THREE.Vector3();
	var scaledVelocity = new THREE.Vector3();
	var friction = new THREE.Vector3();
	
	var lookMatrix = new THREE.Matrix4();
	var meshMatrix = new THREE.Matrix4();
	var rightVector = new THREE.Vector3();
	var cameraOrbit = new THREE.Vector2();
	
	var moving = false;
	var groundFace = null;
	
	/**
	 * This function integrates velocity as follows:
	 * velocity += acceleration * delta / 2;
	 * position += velocity * delta;
	 * velocity += acceleration * delta / 2;
	 * 
	 * @param {number} delta - time elapsed since last update
	 */
	this.update = function( delta ) {
		centerPoint.set(0, 0, this.mesh.position.z);
		gravityVector.subVectors(this.mesh.position, centerPoint).normalize()
		scaledGravity.copy(gravityVector).multiplyScalar( 500 * delta );
		
		velocity.add( scaledGravity );
		
		scaledVelocity.copy(velocity).multiplyScalar(delta);
		this.moveWithCollisions( scaledVelocity );
		
		velocity.add( scaledGravity );
		
		this.followCamera();
//		this.movementControls( delta );
		this.movementTarget( delta );
		
		if( groundFace ) {
			friction.copy( velocity ).normalize().multiplyScalar(-4000 * delta);
			if( friction.lengthSq() > velocity.lengthSq() )
				velocity.set(0, 0, 0);
			else
				velocity.add(friction);
		}
		
		if( velocity.lengthSq > 1 ) {
			velocity.normalize().multiplyScalar( 1 );
		}
	};
	
	/**
	 * Move the villager by vector, colliding with any physics objects
	 * 
	 * @param {THREE.Vector3} vector
	 */
	this.moveWithCollisions = function( vector ) {
		var collision;
		groundFace = null;
		var collisionCount = 10;

		do {
			collision = CollisionCheck(this.mesh.position, vector, sharedData.physicsObjects);
			
			if( collision ) {
				this.mesh.position = collision.intersectionPoint;
				vector.multiplyScalar(1 - collision.parameter).projectOnPlane( collision.face.normal );
				groundFace = collision.face;
			}
		} while( collision && collisionCount-- > 0 )
		
		if( groundFace ) {
			velocity.projectOnPlane( groundFace.normal )
		}
		
		this.mesh.position.add(vector);
	};
	
	this.followCamera = function() {
		gravityVector.multiplyScalar(-1);
		
//		if( sharedData.controls.mouse.locked ) {
//			cameraOrbit.x -= sharedData.controls.mouse.motion.x * 0.001;
//			cameraOrbit.y += sharedData.controls.mouse.motion.y * 0.001;
//		}
//		
//		sharedData.camera.position.set( 0, 100, 100 );
//		sharedData.camera.position.applyAxisAngle( gravityVector, cameraOrbit.x );
//		
//		rightVector.crossVectors(gravityVector, sharedData.camera.position).normalize();
//		sharedData.camera.position.applyAxisAngle( rightVector, cameraOrbit.y );
//		sharedData.camera.position.add( this.mesh.position );
		
//		lookMatrix.lookAt( sharedData.camera.position, this.mesh.position, gravityVector );
//		sharedData.camera.quaternion.setFromRotationMatrix( lookMatrix );
		
		meshMatrix.lookAt(this.mesh.position, this.mesh.position, gravityVector);
		this.mesh.quaternion.setFromRotationMatrix( meshMatrix );
	}
	
	var movementVector = new THREE.Vector3();
	/**
	 * Handle input and add movement to villager velocity
	 */
	this.movementControls = function( delta ) {
		movementVector.set( 0, 0, 0 );
		moving = false;
		
		if( sharedData.controls.keys[38] || sharedData.controls.mouse.buttons[0] ) {		// UP
			movementVector.z -= 1;
			moving = true;
		}
		if( sharedData.controls.keys[40] ) {		// DOWN
			movementVector.z += 1;
			moving = true;
		}
		if( sharedData.controls.keys[37] ) {		// LEFT
			movementVector.x -= 1;
			moving = true;
		}
		if( sharedData.controls.keys[39] ) {		// RIGHT
			movementVector.x += 1;
			moving = true;
		}
		
		if( moving ) {
			movementVector.applyMatrix4( lookMatrix );
			if( groundFace ) {
				movementVector.projectOnPlane( groundFace.normal );
			}
			var movementSpeed = 5000;
			if( !groundFace )
				movementSpeed = 500;
			
			movementVector.normalize().multiplyScalar( movementSpeed * delta );
			
			velocity.add( movementVector );
		}
	};
	
	/**
	 * Handle input and add movement to villager velocity
	 */
	this.movementTarget = function( delta ) {
		if( sharedData.unitControls.targetOn ) {
			if( sharedData.unitControls.targetPoint.distanceToSquared( this.mesh.position ) > 2500 ) {
				movementVector.subVectors(sharedData.unitControls.targetPoint, this.mesh.position);
				
				var movementSpeed = 5000;
				if( !groundFace )
					movementSpeed = 500;
				movementVector.normalize().multiplyScalar( movementSpeed * delta );
				
				velocity.add( movementVector );
			}
		}
	};
	
	this.init();
}