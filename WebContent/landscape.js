var landscape = function () {
    var geometry = new THREE.Geometry();
    var material = new THREE.MeshLambertMaterial( { color: 0xdddddd, ambient: 0xdddddd } );
    var mesh = null;
    
    this.init = function() {
        this.terrain = generateTerrain();

        var increment = 2 * Math.PI / 100;

        for( var i = 0; i < 100; i++ ) {
            var x = Math.cos( i * increment );
            var y = Math.sin( i * increment );
            for( var j = 0; j < 100; j++ ) {
                geometry.vertices.push( new THREE.Vector3( x * (2000 + this.terrain[i][j]), y * (2000 + this.terrain[i][j]), j * 100 ) );
            }
        }

        for( var i = 0; i < 99; i++ ) {
            for( var j = 0; j < 99; j++ ) {
                geometry.faces.push(
                    new THREE.Face3( j * 100 + i, j * 100 + i + 1, (j + 1) * 100 + i ),
                    new THREE.Face3( j * 100 + i + 1, (j + 1) * 100 + i + 1, (j + 1) * 100 + i )
                );
            }
        }

        for( var i = 0; i < 99; i++ ) {
            geometry.faces.push(
                new THREE.Face3( 9900 + i, 9900 + i + 1, i ),
                new THREE.Face3( 9900 + i + 1, i + 1, i )
            );
        }

        geometry.computeFaceNormals();
        geometry.computeVertexNormals();
        
        material.shading = THREE.FlatShading;

        mesh = new THREE.Mesh( geometry, material );
        sharedData.scene.add( mesh );
        sharedData.physicsObjects.push( mesh );
    }
    
    function generateTerrain() {
        var terrain = [];
        for( var i = 0; i < 100; i++ ) {
            terrain[i] = [];
        }

        terrain[0][0] = Math.random() * 100;
        terrain[0][99] = Math.random() * 100;

        subdivide( terrain, 6, 3000, 0, 0, 99, 100 );

        return terrain;
    }

    function subdivide( terrain, level, range, x1, y1, x2, y2 ) {
        var top = y2 % 100;

        if( level > 0 ) {
            var midpointX = Math.floor( (x1 + x2) / 2 );
            var midpointY = Math.floor( (y1 + y2) / 2 );

            if( isNaN(terrain[midpointY][x1]) )
                terrain[midpointY][x1] = (terrain[y1][x1] + terrain[top][x1]) / 2 + (Math.random() * range) - (range / 2);
            if( isNaN(terrain[midpointY][x2]) )
                terrain[midpointY][x2] = (terrain[y1][x2] + terrain[top][x2]) / 2 + (Math.random() * range) - (range / 2);
            if( isNaN(terrain[y1][midpointX]) )
                terrain[y1][midpointX] = (terrain[y1][x1] + terrain[y1][x2]) / 2 + (Math.random() * range) - (range / 2);
            if( isNaN(terrain[top][midpointX]) )
                terrain[top][midpointX] = (terrain[top][x1] + terrain[top][x2]) / 2 + (Math.random() * range) - (range / 2);

            terrain[midpointY][midpointX] = (terrain[y1][x1] + terrain[y1][x2] + terrain[top][x1] + terrain[top][x2]) / 4 + (Math.random() * range) - (range / 2);

            level--;
            range /= 2;
            subdivide( terrain, level, range, x1, y1, midpointX, midpointY );
            subdivide( terrain, level, range, midpointX, y1, x2, midpointY );
            subdivide( terrain, level, range, x1, midpointY, midpointX, y2 );
            subdivide( terrain, level, range, midpointX, midpointY, x2, y2 );
        }
        else {
            var height1 = terrain[y1][x1];
            var increment1 = (terrain[top][x1] - height1) / (y2 - y1);
            var height2 = terrain[y1][x2];
            var increment2 = (terrain[top][x2] - height2) / (y2 - y1);

            for( var i = y1; i <= y2; i++ ) {
                var height = height1;
                var increment = (height2 - height1) / (x2 - x1);

                for( var j = x1; j <= x2; j++ ) {
                    terrain[i % 100][j] = height;
                    height += increment;
                }

                height1 += increment1;
                height2 += increment2;
            }
        }
    }
    
    this.update = function( delta ) {
    	//mesh.rotation.z += 0.01 * delta;
    };
    
    this.init();
};