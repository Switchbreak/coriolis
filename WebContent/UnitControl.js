var UnitControl = function() {
	var raycaster = new THREE.Raycaster();
	
	var geometry = new THREE.BoxGeometry(10, 10, 10);
	var material = new THREE.MeshLambertMaterial( { color: 0xffff00 } );
	var mesh = new THREE.Mesh(geometry, material);
	
	var mouseJustPressed = true;
	var bJustPressed = true;
	var fJustPressed = true;
	var centerCoords = new THREE.Vector2(0, 0);
	
	this.targetOn = false;
	this.targetPoint = new THREE.Vector3();
	
	this.init = function() {
		mesh.visible = false;
		
		sharedData.scene.add( mesh );
	};
	
	this.update = function() {
		if( sharedData.controls.mouse.buttons[0] ) {
			if( mouseJustPressed ) {
				mouseJustPressed = false;
				
				var position = this.getMousePointerGroundPosition();
				if( position ) {
					this.setTargetPoint( position );
				}
			}
		}
		else {
			mouseJustPressed = true;
		}
		
		if( sharedData.controls.keys[66] ) {
			if( bJustPressed ) {
				bJustPressed = false;
				
				var position = this.getMousePointerGroundPosition();
				if( position ) {
					this.spawnTent( position );
				}
			}
		}
		else {
			bJustPressed = true;
		}
		
		if( sharedData.controls.keys[70] ) {
			if( fJustPressed ) {
				fJustPressed = false;
				
				var position = this.getMousePointerGroundPosition();
				if( position ) {
					this.spawnFire( position );
				}
			}
		}
		else {
			fJustPressed = true;
		}
	};
	
	this.getMousePointerGroundPosition = function() {
		raycaster.setFromCamera( centerCoords, sharedData.camera );
		
		var intersects = raycaster.intersectObjects(sharedData.physicsObjects);
		if( intersects.length > 0 ) {
			return intersects[0].point;
		}
		
		return null;
	};
	
	this.setTargetPoint = function( position ) {
		this.targetOn = true;
		this.targetPoint.copy( position );
		
		mesh.position.copy( position );
		mesh.visible = true;
	};
	
	this.spawnTent = function( position ) {
		staticEntities.push(new tent( position ));
	};
	
	this.spawnFire = function( position ) {
		entities.push(new fire( position ));
		console.log( "Spawning fire" );
	};
	
	this.init();
};