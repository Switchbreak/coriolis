var FlyCamera = function( camera, input ) {
	/** @type THREE.Camera */
	var camera = camera;
	/** @type inputEngine */
	var input = input;
	
	var MAX_X_ROTATION = Math.PI * 2;
	var MAX_Y_ROTATION = Math.PI / 2;
	
	var centerPoint = new THREE.Vector3();
	var cameraRotation = new THREE.Vector2();
	var cameraEuler = new THREE.Euler();
	var cameraQuaternion = new THREE.Quaternion();
	var rotationMatrix = new THREE.Matrix4();
	
	camera.position.set( 0, -1000, 10000 );
	
	this.update = function( delta ) {
		centerPoint.set(0, 0, camera.position.z);
		centerPoint.subVectors(centerPoint, camera.position).normalize();
		
		if( input.mouse.locked ) {
			cameraRotation.x -= sharedData.controls.mouse.motion.x * 0.001;
			cameraRotation.y += sharedData.controls.mouse.motion.y * 0.001;
			
			if( cameraRotation.y > MAX_Y_ROTATION )
				cameraRotation.y = MAX_Y_ROTATION;
			else if( cameraRotation.y < -MAX_Y_ROTATION )
				cameraRotation.y = -MAX_Y_ROTATION;
			
			if( cameraRotation.x > MAX_X_ROTATION )
				cameraRotation.x -= MAX_X_ROTATION;
			if( cameraRotation.x < 0 )
				cameraRotation.x += MAX_X_ROTATION;
		}
		
		rotationMatrix.lookAt( camera.position, camera.position, centerPoint );
		camera.quaternion.setFromRotationMatrix( rotationMatrix );
		
		cameraEuler.set(-cameraRotation.y, cameraRotation.x, 0, "ZYX");
		cameraQuaternion.setFromEuler(cameraEuler);
		camera.quaternion.multiply( cameraQuaternion );
		
		movementControls( delta );
		
		if( sharedData.controls.keys[36] ) {					// HOME
			camera.position.copy( entities[0].mesh.position );
		}
	};
	
	var movementVector = new THREE.Vector3();
	/**
	 * Handle input and add movement to villager velocity
	 */
	function movementControls( delta ) {
		movementVector.set( 0, 0, 0 );
		var moving = false;
		
		if( sharedData.controls.keys[38] ) {		// UP
			movementVector.z -= 1;
			moving = true;
		}
		if( sharedData.controls.keys[40] ) {		// DOWN
			movementVector.z += 1;
			moving = true;
		}
		if( sharedData.controls.keys[37] ) {		// LEFT
			movementVector.x -= 1;
			moving = true;
		}
		if( sharedData.controls.keys[39] ) {		// RIGHT
			movementVector.x += 1;
			moving = true;
		}
		
		if( moving ) {
			movementVector.applyQuaternion(camera.quaternion);
			
			movementVector.normalize().multiplyScalar( 1000 * delta );
			
			camera.position.add( movementVector );
		}
	};
};