/**
 * 
 */

var centerVector = new THREE.Vector3();
var lookMatrix = new THREE.Matrix4();

var tent = function( position ) {
	var material = new THREE.MeshLambertMaterial({ color: 0xffffff });
	/** @type THREE.Mesh */
	var mesh = null;
	/** @type THREE.Animation */
	var animation = null;
	
	function init() {
		material.skinning = true;
		material.side = THREE.DoubleSide;
		mesh = new THREE.SkinnedMesh(sharedData.loadedGeometries["resources/Tent.json"], material, false);
		
		mesh.scale.set( 50, 50, 50 );
		mesh.position.copy( position );
		
		centerVector.set(0, 0, mesh.position.z);
		centerVector.sub(mesh.position).normalize();
		lookMatrix.lookAt( mesh.position, mesh.position, centerVector );
		mesh.quaternion.setFromRotationMatrix( lookMatrix );
		
		sharedData.scene.add( mesh );
		
		animation = new THREE.Animation(mesh, mesh.geometry.animation);
		animation.loop = false;
		animation.play(0, 1);
		
		window.setTimeout(function() {
			position.z += 500;
			entities.push( new fire( position ) );
			}, 1000);
	}
	
	this.update = function( delta ) {
	};
	
	init();
}