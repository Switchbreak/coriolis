/**
 * 
 */

var centerVector = new THREE.Vector3();
var lookMatrix = new THREE.Matrix4();

var ramps = function() {
	var material = new THREE.MeshLambertMaterial({ color: 0xdddddd });
	/** @type THREE.Mesh */
	var mesh = null;
	
	function init() {
		mesh = new THREE.Mesh(sharedData.loadedGeometries["resources/SpaceRamps.json"], material);
		
		mesh.scale.set( 120, 120, 120 );
		mesh.rotation.set( Math.PI / 2, 0, 0 );
		mesh.position.set( 0, 0, 5000 );
		
		sharedData.scene.add( mesh );
	}
	
	this.update = function( delta ) {
	};
	
	init();
}