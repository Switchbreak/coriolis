/**
 * 
 */

var centerVector = new THREE.Vector3();
var lookMatrix = new THREE.Matrix4();

var tree = function() {
	var material = new THREE.MeshLambertMaterial({ color: 0x00ff00 });
	/** @type THREE.Mesh */
	var mesh = null;
	
	function init() {
		material.shading = THREE.FlatShading;
		mesh = new THREE.Mesh(sharedData.loadedGeometries["resources/Tree.json"], material);
		
		mesh.scale.set( 100, 100, 100 );
		
		
		var latitude = Math.random() * 100;
		var longitude = Math.random() * 100;
		
		var radius = 2000 + sharedData.landscape.terrain[Math.floor(latitude)][Math.floor(longitude)];
		var angle = latitude * 2 * Math.PI / 100;
		
		mesh.position.set(Math.cos(angle) * radius, Math.sin(angle) * radius, longitude * 100);
		
		centerVector.set(0, 0, mesh.position.z);
		centerVector.sub(mesh.position).normalize();
		lookMatrix.lookAt( mesh.position, mesh.position, centerVector );
		mesh.quaternion.setFromRotationMatrix( lookMatrix );
		
		sharedData.scene.add( mesh );
	}
	
	this.update = function( delta ) {
	};
	
	init();
}