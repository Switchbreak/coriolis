/**
 * 
 */

var inputEngine = function() {
	
	this.keys = [];
	this.mouse = {
		locked: false,
		buttons: [],
		motion: new THREE.Vector2(),
	};
	
	var boundMoveCallback;
	var currentMotion = new THREE.Vector2();
	var lockJump = false;
	
	this.init = function() {
		window.addEventListener( "keydown", this.onKeyDown.bind( this ), false );
		window.addEventListener( "keyup", this.onKeyUp.bind( this ), false );

		document.addEventListener("pointerlockchange", this.onLockChange.bind( this ), false);
		document.addEventListener("mozpointerlockchange", this.onLockChange.bind( this ), false);
		document.addEventListener("webkitpointerlockchange", this.onLockChange.bind( this ), false);
		document.addEventListener("mousedown", this.onMouseDown.bind( this ), false);
		document.addEventListener("mouseup", this.onMouseUp.bind( this ), false);
		
		sharedData.renderer.domElement.requestPointerLock = sharedData.renderer.domElement.requestPointerLock ||
		sharedData.renderer.domElement.mozRequestPointerLock ||
		sharedData.renderer.domElement.webkitRequestPointerLock;
		
		boundMoveCallback = this.onMouseMove.bind( this );
		sharedData.renderer.domElement.onclick = this.requestLock.bind( this );
	}
	
	this.update = function() {
		this.mouse.motion.copy( currentMotion );
		currentMotion.set( 0, 0 );
	}
	
	this.requestLock = function() {
		sharedData.renderer.domElement.requestPointerLock();
	}
	
	this.onKeyDown = function( event ) {
		this.keys[event.keyCode] = true;
	}
	
	this.onKeyUp = function( event ) {
		this.keys[event.keyCode] = false;
	}
	
	this.onLockChange = function( event ) {
		if( document.pointerLockElement === sharedData.renderer.domElement ) {
			this.mouse.locked = true;
			
			window.setTimeout(function() { document.addEventListener("mousemove", boundMoveCallback, false); });
			sharedData.renderer.domElement.onclick = null;
		}
		else {
			this.mouse.locked = false;
			document.removeEventListener("mousemove", boundMoveCallback, false);
			sharedData.renderer.domElement.onclick = this.requestLock.bind( this );
		}
	}
	
	this.onMouseMove = function( event ) {
		currentMotion.x += event.movementX || event.mozMovementX || event.webkitMovementX;
		currentMotion.y += event.movementY || event.mozMovementY || event.webkitMovementY;
	}
	
	this.onMouseDown = function( event ) {
		this.mouse.buttons[event.button] = true;
	}
	
	this.onMouseUp = function( event ) {
		this.mouse.buttons[event.button] = false;
	}
	
	this.init();
}